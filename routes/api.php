<?php

use App\Http\Controllers\AlbumController;
use App\Http\Controllers\ArtistController;
use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->group(function () {
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login']);

    Route::middleware('auth:sanctum')->group(function () {
        Route::get('/me', [AuthController::class, 'me']);
        
        Route::prefix('album')->group(function () {  
            Route::get('/', [AlbumController::class, 'index']);
            Route::post('/create', [AlbumController::class, 'create']);
            Route::post('/update/{id}', [AlbumController::class, 'update']);
            Route::get('/show/{id}', [AlbumController::class, 'show']);
            Route::delete('/delete/{id}', [AlbumController::class, 'delete']);
        });

        Route::prefix('artist')->group(function () {
            Route::get('/', [ArtistController::class, 'index']);
            Route::get('/{id}', [ArtistController::class, 'show']);
        });
    });
});
