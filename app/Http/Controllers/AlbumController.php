<?php

namespace App\Http\Controllers;

use App\Models\Album;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AlbumController extends Controller
{
    private $album;

    public function __construct(Album $album)
    {
        $this->album = $album;
    }

    public function index(Request $request)
    {
        $userId = $request->user()->id;
        $albums = $this->album->where('user_id', $userId)->paginate(10);
        return response()->json($albums);
    }

    public function create(Request $request)
    {
        $data = $request->all();

        $validatedData = Validator::make($data, [
            'name' => 'required|string|max:255',
            'year' => 'required|string',
            'artist_id' => 'required|integer',
        ]);

        if($validatedData->fails()) {
            return response()->json([
                'error' => "true",
                'message' => "Sorry, we couldn't validate your form. Please check you're filled correctly"
            ]);
        }

        $verifyName = $this->album->where('name', $data['name'])->get();
        $verifyArtist = $this->album->where('artist_id', $data['artist_id'])->get(); 

        if(count($verifyName) > 0 && count($verifyArtist) > 0) {
            return response()->json([
                'error' => "true",
                'message' => "Sorry, we couldn't validate your form. Please check you're filled correctly"
            ]);
        } 

        try {

            $album = $this->album->create([
                'name' => $data['name'],
                'year' => $data['year'],
                'user_id' => $request->user()->id,
                'artist_id' => $data['artist_id'],
            ]);

            return response()->json([
                'error' => 'false',
                'message' => 'Album has been created!',
                'album' => $album
            ]);
            
        } catch (\Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    public function update(Request $request, int $id)
    {
        $data = $request->all();
        $album = $this->album->find($id);
        
        if ($album === null) {
            return response()->json([
                'error' => 'true',
                'message' => 'This album don`t exists'
            ], 404);
        }

        try {

            $album->update($data);

            return response()->json([
                'error' => 'false',
                'message' => 'Album has been updated!',
                'album' => $album
            ]);

        } catch (\Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    public function show(int $id)
    {   
        if(!isset($id)) {
            return response()->json([
                'error' => 'true',
                'message' => 'Select some album'
            ], 404); 
        }

        try {

            $album = $this->album->find($id);

            if($album === null) {
                return response()->json([
                    'error' => 'true',
                    'message' => 'This album don`t exists'
                ], 404); 
            }

            return response()->json([
                'error' => 'false',
                'message' => 'Check this album!',
                'album' => $album
            ]);
            
        } catch (\Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    public function delete(Request $request, int $id)
    {
        if(!isset($id)) {
            return response()->json([
                'error' => 'true',
                'message' => 'Select some album'
            ], 404); 
        }

        $userId = $request->user()->id;

        if($userId !== 1) {
            return response()->json([
                'error' => 'true',
                'message' => 'You don`t have permission to delete this album'
            ], 403); 
        }

        try {

            $album = $this->album->find($id);
            $album->delete();

            return response()->json([
                'error' => 'false',
                'message' => 'Album deleted!',
                'album' => $album
            ]);
            
        } catch (\Exception $e) {
            return response()->json($e->getMessage());
        }
    }
}
