<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ArtistController extends Controller
{
    private $artist;

    public function __construct(Artist $artist)
    {
        $this->artist = $artist;
    }

    public function index(Request $request)
    {   
        try {

            $responseJson = Http::accept('application/json')
                                ->withHeaders(['Basic' => 'ZGV2ZWxvcGVyOlpHVjJaV3h2Y0dWeQ=='])
                                ->get('https://moat.ai/api/task/');
            
            return $responseJson;    

        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 400);
        }
    }

    public function show(Request $request, int $id)
    {
        try {
            
            $responseJson = Http::accept('application/json')
                                ->withHeaders(['Basic' => 'ZGV2ZWxvcGVyOlpHVjJaV3h2Y0dWeQ=='])
                                ->get('https://moat.ai/api/task/?artist_id=' . $id);
            
            return $responseJson;          

        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 400);
        }
    }
}
