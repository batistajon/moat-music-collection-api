<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
    
    public function register(Request $request)
    {
        $data = $request->all();

        $validatedData = Validator::make($data, [
            'name' => 'required|string|max:255',
            'user_name' => 'required|string|max:255|unique:users',
            'user_profile_id' => 'required|integer',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
        ]);

        if($validatedData->fails()) {
            return response()->json([
                'error' => "true",
                'message' => "Sorry, we couldn't validate your form. Please check you're filled correctly"
            ]);
        }

        try {
            
            $user = $this->user->create([
                'name' => $data['name'],
                'user_name' => $data['user_name'],
                'user_profile_id' => $data['user_profile_id'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);
                
            return response()->json([
                'error' => 'false',
                'message' => 'User has been created!',
                'user' => $user
            ], 201);
       
        } catch (\Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    public function login(Request $request)
    {
        $credentials = $request->only('user_name', 'password');

        $validatedData = Validator::make($credentials, [
            'user_name' => 'required|string|max:255',
            'password' => 'required|string|min:8',
        ]);

        if($validatedData->fails()) {
            return response()->json([
                'error' => "true",
                'message' => "Sorry, we couldn't validate your form. Please check you're filled correctly"
            ]);
        }

        try {

            $auth = Auth::attempt($credentials);

            if (!$auth) {
                return response()->json([
                    'error' => "true",
                    'message' => "Sorry, we couldn't find an account with this username. Please check you're using the right username and try again."
                ], 401);
            }

            $user = $this->user->where('user_name', $credentials['user_name'])->firstOrFail();

            $token = $user->createToken('auth_token')->plainTextToken;

            return response()->json([
                    'access_token' => $token,
                    'token_type' => 'Bearer',
            ], 200);
            
        } catch (\Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    public function me(Request $request)
    {
        return $request->user();
    }
}
